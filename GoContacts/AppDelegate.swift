//
//  AppDelegate.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    let config = Realm.Configuration(
      // Set the new schema version. This must be greater than the previously used
      // version (if you've never set a schema version before, the version is 0).
      schemaVersion: 4,
      
      // Set the block which will be called automatically when opening a Realm with
      // a schema version lower than the one set above
      migrationBlock: { migration, oldSchemaVersion in
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < 3) {
          // Nothing to do!
          // Realm will automatically detect new properties and removed properties
          // And will update the schema on disk automatically
        }
    })
    Realm.Configuration.defaultConfiguration = config
    
    // Setup default appearances
    UIBarButtonItem.appearance().tintColor = UIColor(hex: Colors.imageTint.rawValue)

    return true
  }
}

