//
//  Colors.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import Foundation

enum Colors: Int {
  case imageTint = 0x50E3C2
  case titleTint = 0x4A4A4A
  case gradientStart = 0xFFFFFF
  case gradientStop = 0xE5F5F0
  case buttonTint = 0x8CE3C2
}
