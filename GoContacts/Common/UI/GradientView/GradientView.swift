//
//  GradientView.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 09/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import UIKit

@IBDesignable
open class GradientView: UIView {
  @IBInspectable
  public var startColor: UIColor = UIColor(hex: Colors.gradientStart.rawValue) {
    didSet {
      gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
      setNeedsDisplay()
    }
  }
  @IBInspectable
  public var endColor: UIColor = UIColor(hex: Colors.gradientStop.rawValue) {
    didSet {
      gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
      setNeedsDisplay()
    }
  }
  
  public lazy var gradientLayer: CAGradientLayer = {
    let gradientLayer = CAGradientLayer()
    gradientLayer.frame = self.bounds
    gradientLayer.colors = [self.startColor.cgColor, self.endColor.cgColor]
    return gradientLayer
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    layer.insertSublayer(gradientLayer, at: 0)
  }
  
  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    layer.insertSublayer(gradientLayer, at: 0)
  }
  
  open override func layoutSubviews() {
    super.layoutSubviews()
    gradientLayer.frame = bounds
  }
}
