//
//  RoundButton.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 09/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import UIKit

@IBDesignable
public class RoundButton: UIButton {

  public override init(frame: CGRect) {
    super.init(frame: frame)
   
    backgroundColor = UIColor(hex: Colors.buttonTint.rawValue)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    backgroundColor = UIColor(hex: Colors.buttonTint.rawValue)
  }
  
  @IBInspectable var borderColor: UIColor = UIColor.white {
    didSet {
      layer.borderColor = borderColor.cgColor
    }
  }
  
  @IBInspectable var borderWidth: CGFloat = 2.0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }
  
  public override var isHighlighted: Bool {
    didSet {
      backgroundColor = isHighlighted ? UIColor.white : UIColor(hex: Colors.buttonTint.rawValue)
      tintColor = isHighlighted ? UIColor(hex: 0xFEFEFE) : UIColor.white
    }
  }
  
  override public func layoutSubviews() {
    super.layoutSubviews()
    layer.cornerRadius = 0.5 * bounds.size.width
    clipsToBounds = true
  }

}
