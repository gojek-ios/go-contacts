//
//  ContactItemCell.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import UIKit

class ContactItemCell: UITableViewCell {
  
  @IBOutlet weak var imageProfile: UIImageView!
  @IBOutlet weak var labelName: UILabel!
  @IBOutlet weak var imageFavorite: UIImageView!
  
  var isFavorite: Bool {
    get {
      return !imageFavorite.isHidden
    }
    set (value) {
      imageFavorite.isHidden = value
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    if let imageProfile = imageProfile {
      let bounds = imageProfile.bounds
      imageProfile.contentMode = .scaleAspectFit
      imageProfile.layer.cornerRadius = bounds.width * 0.5
      imageProfile.layer.masksToBounds = true
    }
    
    if let imageFavorite = imageFavorite {
      imageFavorite.isHidden = true
      imageFavorite.tintColor = UIColor(hex: Colors.imageTint.rawValue)
    }
  }
}
