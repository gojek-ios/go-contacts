//
//  Network.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

protocol Network {
  func request<T: TargetType>(_ target: T, completion: @escaping Completion)
}

class NetworkProvider: Network {
  let provider = MoyaProvider<MultiTarget>()
  
  init() {}
  
  func request<T: TargetType>(_ target: T, completion: @escaping Completion) {
    provider.request(MultiTarget(target), completion: completion)
  }  
}
