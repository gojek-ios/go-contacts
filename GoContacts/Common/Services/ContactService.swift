//
//  ContactService.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import Foundation
import Moya

enum ContactService {
  case getAllContacts
  case getContact(id: Int)
  case createContact(firstName: String, lastName: String, email: String, phone: String)
  case updateFavorite(id: Int, favorite: Bool)
  case updateContact(id: Int, firstName: String, lastName: String, email: String, phone: String)
  case deleteContact(id: Int)
  
  static var BASE_URL: String {
    return "http://gojek-contacts-app.herokuapp.com"
  }
}

extension ContactService: TargetType {
  var baseURL: URL {
    return URL(string: ContactService.BASE_URL)!
  }
  
  var path: String {
    switch self {
    case .getAllContacts,
         .createContact(_, _, _, _):
      return "/contacts.json"

    case .getContact(let id),
         .updateFavorite(let id, _),
         .updateContact(let id, _, _, _, _),
         .deleteContact(let id):
      return "/contacts/\(id).json"
    }
  }
  
  var method: Moya.Method {
    switch self {
    case .getAllContacts,
         .getContact(_):
      return .get
      
    case .createContact:
      return .post
      
    case .updateFavorite(_, _),
         .updateContact(_, _, _, _, _):
      return .put
      
    case .deleteContact(_):
      return .delete
    }
  }
  
  var sampleData: Data {
    return Data()
  }
  
  var task: Task {
    switch self {
    case .getAllContacts,
         .getContact(_):
      return .requestPlain

    case .updateFavorite(_, let favorite):
      return .requestParameters(parameters: ["favorite": favorite], encoding: JSONEncoding.default)
      
    case .updateContact(_, let firstName, let lastName, let email, let phone):
      return .requestParameters(parameters: ["first_name": firstName, "last_name": lastName, "email": email, "phone_number": phone],
                                encoding: JSONEncoding.default)
      
    case .createContact(let firstName, let lastName, let email, let phone):
      return .requestParameters(parameters: ["first_name": firstName, "last_name": lastName, "email": email, "phone_number": phone],
                                encoding: JSONEncoding.default)
      
    case .deleteContact(_):
      return .requestPlain
    }
  }
  
  var headers: [String : String]? {
    return ["Content-Type": "application/json"]
  }
  
}
