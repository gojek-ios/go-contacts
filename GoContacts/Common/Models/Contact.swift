//
//  Contact.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import Foundation
import RealmSwift

class Contact: Object, Decodable {
  @objc dynamic var id: Int
  @objc dynamic var first_name: String
  @objc dynamic var last_name: String
  @objc dynamic var profile_pic: String
  @objc dynamic var favorite: Bool
  @objc dynamic var url: String
  
  override static func primaryKey() -> String? {
    return "id"
  }
}
