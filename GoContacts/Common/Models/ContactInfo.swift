//
//  ContactDetail.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 09/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import Foundation
import RealmSwift

class ContactInfo: Object, Decodable {
  @objc dynamic var id: Int
  @objc dynamic var first_name: String
  @objc dynamic var last_name: String
  @objc dynamic var profile_pic: String
  @objc dynamic var favorite: Bool
  @objc dynamic var email: String?
  @objc dynamic var phone_number: String?
    
  override static func primaryKey() -> String? {
    return "id"
  }
}

