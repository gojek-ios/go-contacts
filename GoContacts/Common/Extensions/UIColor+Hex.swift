//
//  UIColor+Hex.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import UIKit

extension UIColor {
  convenience init(hex: Int) {
    self.init(hex: hex, alpha: 1.0)
  }
  
  convenience init(hex: Int, alpha: CGFloat) {
    self.init(
      red: CGFloat(CGFloat((hex & 0xFF0000) >> 16) / 255.0),
      green: CGFloat(CGFloat((hex & 0xFF00) >> 8) / 255.0),
      blue: CGFloat(CGFloat(hex & 0xFF) / 255.0),
      alpha: alpha)
  }
}
