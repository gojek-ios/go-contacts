//
//  Results+toArray.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import RealmSwift

extension Results {
  func toArray() -> [Element] {
    return compactMap { $0 }
  }
}
