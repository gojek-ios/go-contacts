//
//  UIView+Fx.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 09/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import UIKit

extension UIView {
  func applyRoundedCorner(radius: CGFloat) {
    self.layer.cornerRadius = radius
    self.layer.masksToBounds = true
  }
  
  func applyBorder(width: CGFloat, color: CGColor) {
    self.layer.borderWidth = width
    self.layer.borderColor = color
  }  
}
