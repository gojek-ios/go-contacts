//
//  HomeRouterTests.swift
//  GoContactsTests
//
//  Created by Suhendra Ahmad on 10/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

@testable import GoContacts
import XCTest

class HomeRouterTests: XCTestCase {

  var window: UIWindow!
  var sut: HomeRouter!
  var source: HomeViewController!
  var contactEditDestination: ContactEditViewController!
  var contactDetailDestination: ContactDetailViewController!
  
  // MARK: Test lifecycle
  
  override func setUp()
  {
    super.setUp()
    window = UIWindow()
    setupHomeRouter()
  }
  
  override func tearDown()
  {
    window = nil
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func loadView()
  {
    window.addSubview(source.view)
    RunLoop.current.run(until: Date())
  }
  
  func setupHomeRouter()
  {
    sut = HomeRouter()
    let bundle = Bundle.main
    let storyboard = UIStoryboard(name: "Main", bundle: bundle)
    source = (storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController)
    contactEditDestination = (storyboard.instantiateViewController(withIdentifier: "ContactEditViewController") as! ContactEditViewController)
    contactDetailDestination = (storyboard.instantiateViewController(withIdentifier: "ContactDetailViewController") as! ContactDetailViewController)
  }
  
  // MARK: Test doubles
  
  
  // MARK: Tests
  
  func testNavigateToContactEdit()
  {
    // Given
    
    // When
    loadView()
    
    let previousPresentedVC = source.presentedViewController
    sut.navigateToContactEdit(source: source, destination: contactEditDestination)
    let currentPresentedVC = source.presentedViewController
    
    XCTAssertNotEqual(previousPresentedVC, currentPresentedVC)
  }
  
  
//  func testPassDataToContactEdit()
//  {
//    // Given
//    loadView()
//    destination.viewDidLoad()
//
//    let sourceDataSource = source.router!.dataStore!
//    var destinationDataSource = destination.router!.dataStore!
//
//    // When
//    sut.passDataToContactEdit(source: sourceDataSource, destination: &destinationDataSource)
//
//    XCTAssertEqual(destinationDataSource.interactionType, .editContact)
//    XCTAssertEqual(destinationDataSource.contactId, sourceDataSource.contactId)
//    XCTAssertEqual(destinationDataSource.contactFirstName, sourceDataSource.contactFirstName)
//    XCTAssertEqual(destinationDataSource.contactLastName, sourceDataSource.contactLastName)
//    XCTAssertEqual(destinationDataSource.contactEmail, sourceDataSource.contactEmail)
//    XCTAssertEqual(destinationDataSource.contactPhoneNumber, sourceDataSource.contactPhone)
//    XCTAssertEqual(destinationDataSource.contactProfilePicture, sourceDataSource.contactProfilePicture)
//  }

  func testRouteToAddContact()
  {
    // Given
    loadView()
    let _ = contactEditDestination.view
    sut.viewController = source
    sut.dataStore = (source.interactor as! HomeDataStore)

    // When
    let previousPresentedVC = source.presentedViewController
    sut.routeToAddContact()
    let currentPresentedVC = source.presentedViewController

    XCTAssertNotEqual(previousPresentedVC, currentPresentedVC)
  }
  
  func testRouteToDetailContact()
  {
    // Given
    loadView()
    let _ = contactDetailDestination.view
    sut.viewController = source
    sut.dataStore = (source.interactor as! HomeDataStore)
    let detailDataStore = (contactDetailDestination.interactor as! ContactDetailDataStore)
    
    // When
    sut.routeToDetailContact()
    
    XCTAssertEqual(sut.dataStore!.id, detailDataStore.id)
  }
}
