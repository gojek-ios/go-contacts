//
//  HomePresenterTests.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 08/03/2019.
//  Copyright (c) 2019 Suhendra Ahmad. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

@testable import GoContacts
import XCTest

class HomePresenterTests: XCTestCase
{
  // MARK: Subject under test
  
  var sut: HomePresenter!
  
  // MARK: Test lifecycle
  
  override func setUp()
  {
    super.setUp()
    setupHomePresenter()
  }
  
  override func tearDown()
  {
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func setupHomePresenter()
  {
    sut = HomePresenter()
  }
  
  // MARK: Test doubles
  
  class HomeDisplayLogicSpy: HomeDisplayLogic
  {
    var displayContactsCalled = false
    
    func displayContacts(viewModels: [Home.ContactList.ViewModel]) {
      displayContactsCalled = true
    }
  }
  
  // MARK: Tests
  
  func testPresentSomething()
  {
    // Given
    let spy = HomeDisplayLogicSpy()
    sut.viewController = spy
    let response = Home.ContactList.Response(contacts: [])
    
    // When
    sut.presentContacts(response: response)
    
    // Then
    XCTAssertTrue(spy.displayContactsCalled, "presentContacts(response:) should ask the view controller to display the result")
  }
}
