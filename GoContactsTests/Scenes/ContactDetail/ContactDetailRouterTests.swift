//
//  ContactDetailRouterTests.swift
//  GoContactsTests
//
//  Created by Suhendra Ahmad on 10/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

@testable import GoContacts
import XCTest

class ContactDetailRouterTests: XCTestCase
{
  
  // MARK: Subject under test
  
  var window: UIWindow!
  var sut: ContactDetailRouter!
  var json: Data!
  var info: ContactInfo!
  var source: ContactDetailViewController!
  var destination: ContactEditViewController!
  
  // MARK: Test lifecycle
  
  override func setUp()
  {
    super.setUp()
    window = UIWindow()
    setupContactDetailRouter()
  }
  
  override func tearDown()
  {
    window = nil
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func loadView()
  {
    window.addSubview(source.view)
    RunLoop.current.run(until: Date())
  }
  
  func setupContactDetailRouter()
  {
    sut = ContactDetailRouter()
    let bundle = Bundle.main
    let storyboard = UIStoryboard(name: "Main", bundle: bundle)
    source = (storyboard.instantiateViewController(withIdentifier: "ContactDetailViewController") as! ContactDetailViewController)
    destination = (storyboard.instantiateViewController(withIdentifier: "ContactEditViewController") as! ContactEditViewController)
  }
  
  // MARK: Test doubles
  
  
  // MARK: Tests
  
  func testNavigateToContactEdit()
  {
    // Given
    
    // When
    loadView()
    
    let previousPresentedVC = source.presentedViewController
    sut.navigateToContactEdit(source: source, destination: destination)
    let currentPresentedVC = source.presentedViewController
    
    XCTAssertNotEqual(previousPresentedVC, currentPresentedVC)
  }
  
  func testPassDataToContactEdit()
  {
    // Given
    loadView()
    destination.viewDidLoad()
    
    let sourceDataSource = source.router!.dataStore!
    var destinationDataSource = destination.router!.dataStore!
    
    // When
    sut.passDataToContactEdit(source: sourceDataSource, destination: &destinationDataSource)
    
    XCTAssertEqual(destinationDataSource.interactionType, .editContact)
    XCTAssertEqual(destinationDataSource.contactId, sourceDataSource.contactId)
    XCTAssertEqual(destinationDataSource.contactFirstName, sourceDataSource.contactFirstName)
    XCTAssertEqual(destinationDataSource.contactLastName, sourceDataSource.contactLastName)
    XCTAssertEqual(destinationDataSource.contactEmail, sourceDataSource.contactEmail)
    XCTAssertEqual(destinationDataSource.contactPhoneNumber, sourceDataSource.contactPhone)
    XCTAssertEqual(destinationDataSource.contactProfilePicture, sourceDataSource.contactProfilePicture)
  }
  
  func testRouteToContactEdit()
  {
    // Given
    loadView()
    let _ = destination.view
    sut.viewController = source
    sut.dataStore = (source.interactor as! ContactDetailDataStore)
    
    // When
    let previousPresentedVC = source.presentedViewController
    sut.routeToContactEdit()
    let currentPresentedVC = source.presentedViewController
    
    XCTAssertNotEqual(previousPresentedVC, currentPresentedVC)
  }
}
