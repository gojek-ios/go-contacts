//
//  ContactDetailInteractorTests.swift
//  GoContacts
//
//  Created by Suhendra Ahmad on 10/03/2019.
//  Copyright (c) 2019 Suhendra Ahmad. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

@testable import GoContacts
import XCTest

class ContactDetailInteractorTests: XCTestCase
{
  // MARK: Subject under test
  
  var sut: ContactDetailInteractor!
  var info: ContactInfo!
  
  // MARK: Test lifecycle
  
  override func setUp()
  {
    super.setUp()
    setupContactDetailInteractor()
    
    let json = """
    {
      "id": 1,
      "first_name": "Suhendra",
      "last_name": "Ahmad",
      "favorite": false,
      "profile_pic": "/images/missing.png",
      "email": "a@b.com",
      "phone_number": "012312313"
    }
    """.data(using: .utf8)!
    info = try! JSONDecoder().decode(ContactInfo.self, from: json)
  }
  
  override func tearDown()
  {
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func setupContactDetailInteractor()
  {
    sut = ContactDetailInteractor()
  }
  
  // MARK: Test doubles
  
  class ContactDetailPresentationLogicSpy: ContactDetailPresentationLogic
  {
    var presentContactDetailsCalled = false
    var presentContactEditCalled = false
    
    func presentContactDetails(response: ContactDetail.Info.Response) {
      presentContactDetailsCalled = true
    }
    
    func presentContactEdit(response: ContactDetail.Edit.Response) {
      presentContactEditCalled = true
    }
  }
  
  class ContactDetailWorkerStub: ContactDetailWorker
  {
    let info: ContactInfo!
    init(_ info: ContactInfo)
    {
      self.info = info
      super.init(network: NetworkProvider())
    }
    
    override func fetchDetails(id: Int, onSuccess: @escaping ContactDetailSuccessHandler, onError: ContactDetailErrorHandler?) {
      onSuccess(info)
    }
    
    override func update(id: Int, favorite: Bool, onSuccess: @escaping ContactDetailSuccessHandler, onError: ContactDetailErrorHandler?) {
      onSuccess(info)
    }
  }
  
  // MARK: Tests
  
  func testFetchDetails()
  {
    // Given
    let spy = ContactDetailPresentationLogicSpy()
    sut.presenter = spy
    let workerStub = ContactDetailWorkerStub(info)
    sut.worker = workerStub
    let request = ContactDetail.Info.Request()
      
    // When
    sut.fetchDetails(request: request)
    
    XCTAssertTrue(spy.presentContactDetailsCalled)
  }

  func testUpdateFavorite()
  {
    // Given
    let spy = ContactDetailPresentationLogicSpy()
    sut.presenter = spy
    let workerStub = ContactDetailWorkerStub(info)
    sut.worker = workerStub
    let request = ContactDetail.Info.UpdateRequest(favorite: true)
    
    // When
    sut.updateFavorite(request: request)
    
    XCTAssertTrue(spy.presentContactDetailsCalled)
  }
  
  func testEditContact()
  {
    // Given
    let spy = ContactDetailPresentationLogicSpy()
    sut.presenter = spy
    let request = ContactDetail.Edit.Request(id: info.id,
                                             firstName: info.first_name,
                                             lastName: info.last_name,
                                             email: info.email!,
                                             phone: info.phone_number!,
                                             profilePicture: info.profile_pic)
    
    // When
    sut.editContact(request: request)
    
    XCTAssertTrue(spy.presentContactEditCalled)
    XCTAssertEqual(request.id, sut.contactId)
    XCTAssertEqual(request.firstName, sut.contactFirstName)
    XCTAssertEqual(request.lastName, sut.contactLastName)
    XCTAssertEqual(request.email, sut.contactEmail)
    XCTAssertEqual(request.phone, sut.contactPhone)
    XCTAssertEqual(request.profilePicture, sut.contactProfilePicture)
  }
  
//  func testDoSomething()
//  {
//    // Given
//    let spy = ContactDetailPresentationLogicSpy()
//    sut.presenter = spy
//    let request = ContactDetail.Info.Request()
//
//    // When
//    sut.doSomething(request: request)
//
//    // Then
//    XCTAssertTrue(spy.presentSomethingCalled, "doSomething(request:) should ask the presenter to format the result")
//  }
}
