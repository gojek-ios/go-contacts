//
//  RoundButtonTests.swift
//  GoContactsTests
//
//  Created by Suhendra Ahmad on 10/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import Quick
import Nimble

@testable import GoContacts

class RoundButtonTests: QuickSpec {
  
  override func spec() {
    
    describe("RoundButton") {
      
      var view: RoundButton!
      
      beforeEach {
        view = RoundButton(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
      }
      
      it("should have default backgroundColor to Colors.buttonTint") {
        expect(view.backgroundColor).to(equal(UIColor(hex: Colors.buttonTint.rawValue)))
      }
      
      it("should set layer borderColor") {
        view.borderColor = UIColor.white
        expect(view.layer.borderColor!).to(equal(UIColor.white.cgColor))
      }
      
      it("should set layer border width") {
        let width: CGFloat = 2.0
        view.borderWidth = width
        expect(view.layer.borderWidth).to(equal(width))
      }
      
      it("should set highlight color") {
        view.isHighlighted = true
        expect(view.backgroundColor).to(equal(UIColor.white))
        view.isHighlighted = false
        expect(view.backgroundColor).to(equal(UIColor(hex: Colors.buttonTint.rawValue)))
      }
      
      it("should update corner radius upon layout") {
        view.layoutSubviews()
        expect(view.layer.cornerRadius).to(equal(0.5 * view.bounds.width))
      }
    }
    
  }

}
