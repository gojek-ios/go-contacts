//
//  ContactItemCellTests.swift
//  GoContactsTests
//
//  Created by Suhendra Ahmad on 10/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import Quick
import Nimble

@testable import GoContacts

class ContactItemCellTests: QuickSpec {

  override func spec() {
    
    var cell: ContactItemCell!
    
    beforeEach {
      cell = (Bundle.main.loadNibNamed("ContactItemCell", owner: nil, options: nil)?.first as! ContactItemCell)
    }
    
    it("should show/hide image according to isFavorite") {
      cell.isFavorite = true
      expect(cell.imageFavorite.isHidden).to(equal(true))
      expect(cell.imageFavorite.isHidden).to(equal(!cell.isFavorite))
    }
  }

}
