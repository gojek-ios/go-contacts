//
//  GradientViewTests.swift
//  GoContactsTests
//
//  Created by Suhendra Ahmad on 10/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import Quick
import Nimble

@testable import GoContacts

class GradientViewTests: QuickSpec {

  override func spec() {
    describe("GradientView") {
      
      var view: GradientView!
      
      beforeEach {
        view = GradientView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
      }
      
      it("should set gradientLayer colors' startColor") {
        view.startColor = UIColor.yellow
        let colors = view.gradientLayer.colors as! [CGColor]
        expect(colors.count).to(equal(2))
        expect(colors[0]).to(equal(UIColor.yellow.cgColor))
      }
      
      it("should set gradient colors' endColor") {
        view.endColor = UIColor.yellow
        let colors = view.gradientLayer.colors as! [CGColor]
        expect(colors.count).to(equal(2))
        expect(colors[1]).to(equal(UIColor.yellow.cgColor))
      }
      
      it("should update gradientLayer frame") {
        view.layoutSubviews()
        expect(view.gradientLayer.frame).to(equal(view.bounds))
      }
    }
  }

}
