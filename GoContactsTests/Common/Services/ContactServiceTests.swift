//
//  ContactServiceTests.swift
//  GoContactsTests
//
//  Created by Suhendra Ahmad on 09/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import XCTest
@testable import GoContacts
@testable import Moya

class ContactServiceTests: XCTestCase {
  
  override func setUp() {
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }
  
  func testBaseUrl() {
    let expectedUrl = "http://gojek-contacts-app.herokuapp.com"
    let baseUrl = ContactService.BASE_URL
    let request = ContactService.getAllContacts
    
    XCTAssertEqual(expectedUrl, baseUrl)
    XCTAssertEqual(expectedUrl, request.baseURL.absoluteString)
  }
  
  func testMethod() {
    XCTAssertEqual(.get, ContactService.getAllContacts.method)
    XCTAssertEqual(.get, ContactService.getContact(id: 0).method)
    XCTAssertEqual(.post, ContactService.createContact(firstName: "", lastName: "", email: "", phone: "").method)
    XCTAssertEqual(.put, ContactService.updateFavorite(id: 0, favorite: true).method)
    XCTAssertEqual(.delete, ContactService.deleteContact(id: 0).method)
  }
  
  func testHeaders() {
    let expectedHeaders = ["Content-Type": "application/json"]
    XCTAssertEqual(expectedHeaders, ContactService.getAllContacts.headers)
    XCTAssertEqual(expectedHeaders, ContactService.getContact(id: 0).headers)
    XCTAssertEqual(expectedHeaders, ContactService.createContact(firstName: "", lastName: "", email: "", phone: "").headers)
    XCTAssertEqual(expectedHeaders, ContactService.updateFavorite(id: 0, favorite: true).headers)
    XCTAssertEqual(expectedHeaders, ContactService.deleteContact(id: 0).headers)
  }
  
  func testData() {
    let expectedData = Data()
    XCTAssertEqual(expectedData, ContactService.getAllContacts.sampleData)
    XCTAssertEqual(expectedData, ContactService.getContact(id: 0).sampleData)
    XCTAssertEqual(expectedData, ContactService.createContact(firstName: "", lastName: "", email: "", phone: "").sampleData)
    XCTAssertEqual(expectedData, ContactService.updateFavorite(id: 0, favorite: true).sampleData)
    XCTAssertEqual(expectedData, ContactService.deleteContact(id: 0).sampleData)
  }
  
  func testPath() {
    let expectedPath = "/contacts.json"
    let expectedParamPath = "/contacts/0.json"
    
    XCTAssertEqual(expectedPath, ContactService.getAllContacts.path)
    XCTAssertEqual(expectedPath, ContactService.createContact(firstName: "", lastName: "", email: "", phone: "").path)
    XCTAssertEqual(expectedParamPath, ContactService.getContact(id: 0).path)
    XCTAssertEqual(expectedParamPath, ContactService.updateFavorite(id: 0, favorite: true).path)
    XCTAssertEqual(expectedParamPath, ContactService.deleteContact(id: 0).path)
  }  
}
