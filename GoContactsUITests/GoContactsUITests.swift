//
//  GoContactsUITests.swift
//  GoContactsUITests
//
//  Created by Suhendra Ahmad on 10/03/2019.
//  Copyright © 2019 Suhendra Ahmad. All rights reserved.
//

import XCTest

class GoContactsUITests: XCTestCase {
  
  override func setUp() {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false
    
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication().launch()
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }
  
  func waitForElementToAppear(element: XCUIElement, timeout: TimeInterval = 5,  file: String = #file, line: UInt = #line) {
    let existsPredicate = NSPredicate(format: "exists == true")
    
    expectation(for: existsPredicate,
                evaluatedWith: element, handler: nil)
    
    waitForExpectations(timeout: timeout) { (error) -> Void in
      if (error != nil) {
        let message = "Failed to find \(element) after \(timeout) seconds."
        self.recordFailure(withDescription: message, inFile: file, atLine: Int(line), expected: true)
      }
    }
  }
  
  func testAddContact() {
    
    let app = XCUIApplication()
    app.navigationBars["Contact"].buttons["Add"].tap()
    
    let tablesQuery = app.tables
    
    let firstNameTextField = tablesQuery.cells.containing(.staticText, identifier:"First Name").children(matching: .textField).element
    firstNameTextField.tap()
    firstNameTextField.typeText("AAA-UI")
    
    let lastNameTextField = tablesQuery.cells.containing(.staticText, identifier:"Last Name").children(matching: .textField).element
    lastNameTextField.tap()
    lastNameTextField.typeText("TEST")
    
    let mobileTextField = tablesQuery.cells.containing(.staticText, identifier:"mobile").children(matching: .textField).element
    mobileTextField.tap()
    mobileTextField.typeText("0137527447")
    
    let emailTextField = tablesQuery.cells.containing(.staticText, identifier:"email").children(matching: .textField).element
    emailTextField.tap()
    emailTextField.typeText("hello@good.bye")
    
    app.navigationBars["GoContacts.ContactEditView"].buttons["Done"].tap()
  }
  
  func testDetailContact() {
    let app = XCUIApplication()
    let tablesQuery = app.tables
    
    let rowElements = tablesQuery.staticTexts.matching(identifier: "AAA-UI TEST")
    if (rowElements.count > 0) {
      let firstRowElement = rowElements.element(boundBy: 0)
      firstRowElement.tap()
    } else {
      XCTFail("You need to run testAddContact before start this test")
    }
    
    let waitEl = app.otherElements.containing(.navigationBar, identifier:"GoContacts.ContactDetailView").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
    waitForElementToAppear(element: waitEl, timeout: 5.0)
    waitEl.tap()
    
    XCTAssertTrue(app.staticTexts["AAA-UI TEST"].exists)
    XCTAssertTrue(tablesQuery.staticTexts["0137527447"].exists)
    XCTAssertTrue(tablesQuery.staticTexts["hello@good.bye"].exists)
    
    let navBar = app.navigationBars["GoContacts.ContactDetailView"]
    navBar.buttons["Edit"].tap()
    
    let firstName = tablesQuery.cells.containing(.staticText, identifier:"First Name").children(matching: .textField).element
    XCTAssertTrue(firstName.exists)
    
    let lastName = tablesQuery.cells.containing(.staticText, identifier:"Last Name").children(matching: .textField).element
    XCTAssertTrue(lastName.exists)
    
    let mobile = tablesQuery.cells.containing(.staticText, identifier:"mobile").children(matching: .textField).element
    XCTAssertTrue(mobile.exists)
    
    let email = tablesQuery.cells.containing(.staticText, identifier:"email").children(matching: .textField).element
    XCTAssertTrue(email.exists)
    
    app.navigationBars["GoContacts.ContactEditView"].buttons["Done"].tap()
    navBar.buttons["Contact"].tap()
  }
  
}
