# Go Contacts
GOJEK Contacts iOS Challenge

## Architecture 
Clean Architecture with VIP, using [clean-swift](https://clean-swift.com/)

## How to run
You will need [Cocoapods](https://cocoapods.org/) installed on the system, then do the following scripts on terminal.

```bash
$ pod install
```

Then open **GoContacts.xcworkspace** on Xcode, do not open GoContacts.xcodeproj

## Scenes
* Home (Contact List)
* Contact Details
* Contact Edit (Add & Edit Contact)

## Frameworks
* Package manager with [Cocoapods](https://cocoapods.org)
* Reative programming with [RxSwift](https://github.com/ReactiveX/RxSwift)
* Cocoa based Reactive UI with [RxCocoa](https://github.com/ReactiveX/RxSwift/tree/master/RxCocoa)
* TableView reactive programming with [RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources)
* API client with [Moya](https://github.com/Moya/Moya)
* Image caching and loading with [Kingfisher](https://github.com/onevcat/Kingfisher) and [RxKingfisher](https://github.com/RxSwiftCommunity/RxKingfisher)
* Local DB cache with [RealmSwift](https://realm.io/docs/swift/latest/) and [RxRealm](https://github.com/RxSwiftCommunity/RxRealm)
* Testing framework with [Quick](https://github.com/Quick/Quick) and matcher framework using [Nimble](https://github.com/Quick/Nimble)

## Author
Created by Suhendra Ahmad on March 11th 2019

## Repo
=======
* [gitlab.com/azanium](https://gitlab.com/azanium)
* [github.com/azanium](https://github.com/azanium/)
